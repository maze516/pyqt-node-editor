
Welcome to PyQtNodeEditor
==========================

This package was created from the Node Editor Tutorial Series located my website.
The tutorials are published on youtube for free. The full list of tutorials can be located here:
https://www.blenderfreak.com/tutorials/node-editor-tutorial-series/


.. image:: https://readthedocs.org/projects/pyqt-node-editor/badge/?version=latest
    :target: https://pyqt-node-editor.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status


Requirements
------------

- Python 3.x
- PyQt5

Installation
------------

::

    $ pip install git+https://gitlab.com/pavel.krupala/pyqt-node-editor.git


Or download the source code from gitlab::

    git clone https://gitlab.com/pavel.krupala/pyqt-node-editor.git

Other links
-----------

- `Documentation <https://pyqt-node-editor.readthedocs.io/en/latest/>`_

- `Contribute <https://gitlab.com/pavel.krupala/pyqt-node-editor/blob/master/CONTRIBUTING.md>`_

    - `Issues <https://gitlab.com/pavel.krupala/pyqt-node-editor/issues>`_

    - `Merge requests <https://gitlab.com/pavel.krupala/pyqt-node-editor/merge_requests>`_

- `Changelog <https://gitlab.com/pavel.krupala/pyqt-node-editor/blob/master/CHANGES.rst>`_